from api import create_app
from pyspark import SparkContext, SparkConf
import os


def init_spark_context():

    # load spark context
    conf = SparkConf().setAppName("pyspark-flask-example")

    # IMPORTANT: pass aditional Python modules to each worker
    sc = SparkContext(conf=conf, pyFiles=['engine.py', 'api.py'])

    return sc


if __name__ == "__main__":
    # Init spark context and load libraries
    sc = init_spark_context()
    app = create_app(sc)

    port = int(os.environ.get("PORT", 8081))
    app.run(host='0.0.0.0', port=port)
