# https://towardsdatascience.com/multi-class-text-classification-with-pyspark-7d78d022ed35

from pyspark.ml.feature import RegexTokenizer, StopWordsRemover
from pyspark.sql import SQLContext
from pyspark import SparkContext
from pyspark.ml import Pipeline
from pyspark.ml.feature import StringIndexer, IndexToString
from pyspark.ml.feature import HashingTF, IDF
from pyspark.ml.classification import NaiveBayes
from pyspark.sql.types import StringType, StructType, StructField

# Start Spark
sc =SparkContext()
sqlContext = SQLContext(sc)

# Read Data
file_location = "/user/mcarlin/db/slack-data/clean-data.txt"
data = sqlContext.read.format('com.databricks.spark.csv').options(header=True, inferschema='true').load(file_location)


# Regular expression tokenizer
regexTokenizer = RegexTokenizer(inputCol="message", outputCol="words", pattern="\\W")

# Stop words
add_stopwords = ["http","https","amp","rt","t","c","the"]
stopwordsRemover = StopWordsRemover(inputCol="words", outputCol="filtered").setStopWords(add_stopwords)

# TFIDF
hashingTF = HashingTF(inputCol="filtered", outputCol="rawFeatures", numFeatures=100000)
idf = IDF(inputCol="rawFeatures", outputCol="features", minDocFreq=1) #minDocFreq: remove sparse terms

# String Indexer
label_stringIdx = StringIndexer(inputCol = "channel", outputCol = "label")

# Build Pipeline
pipeline = Pipeline(stages=[regexTokenizer, stopwordsRemover, hashingTF, idf, label_stringIdx])
pipelineFit = pipeline.fit(data)
dataset = pipelineFit.transform(data)

###############

# Split Data
(trainingData, testData) = dataset.randomSplit([0.9, 0.1], seed = 100)

# Run Model
nb = NaiveBayes(smoothing=1)
model = nb.fit(trainingData)

##############################################
listy = ["thank you for your help, more pizza please"]
schema = StructType([StructField('message', StringType(), True)])
dfP = sqlContext.createDataFrame(sc.parallelize([listy]), schema)

# Build Pred Pipeline
pipelineP = Pipeline(stages=[regexTokenizer, stopwordsRemover, hashingTF, idf])
pipelineFitP = pipelineP.fit(dfP)
datasetP = pipelineFitP.transform(dfP)

results = model.transform(datasetP)
###############################


meta = [f.metadata for f in dataset.schema.fields if f.name == "label"]