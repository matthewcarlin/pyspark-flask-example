import os
import logging
from pyspark.ml.feature import RegexTokenizer, StopWordsRemover
from pyspark.sql import SQLContext
from pyspark import SparkContext
from pyspark.ml import Pipeline
from pyspark.ml.feature import StringIndexer, IndexToString
from pyspark.ml.feature import HashingTF, IDF
from pyspark.ml.classification import NaiveBayes
from pyspark.sql.types import StringType, StructType, StructField

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class SlackClassificationEngine:
    """A Slack message classification recommendation engine
    """

    def __train_model(self):
        """Train the NB model with the current dataset
        """
        logger.info("Training the NB model...")

        # Transform the Raw Data
        pipeline = self.__build_pipeline()
        pipelineFit = pipeline.fit(self.raw_data)
        dataset = pipelineFit.transform(self.raw_data)

        # Save the labled classes for lookup on prediction
        meta = [f.metadata for f in dataset.schema.fields if f.name == "label"]
        self.labels = dict(enumerate(meta[0]["ml_attr"]["vals"]))

        # Build the model
        nb = NaiveBayes(smoothing=1)
        self.model = nb.fit(dataset)

        logger.info("NB model built!")

    def __build_pipeline(self):
        """Build the processing pipeline to use for training
        """
        # Regular expression tokenizer
        self.regexTokenizer = RegexTokenizer(inputCol="message", outputCol="words", pattern="\\W")

        # Stop words
        self.add_stopwords = ["and", "the"]
        self.stopwordsRemover = StopWordsRemover(inputCol="words", outputCol="filtered").setStopWords(self.add_stopwords)

        # TFIDF
        self.hashingTF = HashingTF(inputCol="filtered", outputCol="rawFeatures", numFeatures=100000)
        self.idf = IDF(inputCol="rawFeatures", outputCol="features", minDocFreq=1)  # minDocFreq: remove sparse terms

        # String Indexer
        self.label_stringIdx = StringIndexer(inputCol="channel", outputCol="label")

        # Build Pipeline
        pipeline = Pipeline(stages=[self.regexTokenizer, self.stopwordsRemover, self.hashingTF, self.idf, self.label_stringIdx])

        return pipeline

    def __build_prediction_pipeline(self):
        """Build the processing pipeline to use for training
        """
        # Build Pipeline
        pipeline = Pipeline(stages=[self.regexTokenizer, self.stopwordsRemover, self.hashingTF, self.idf])

        return pipeline

    def predict_channel(self, slack_message):
        """Gets predictions for a given Slack Message
        Returns: String of the predicted channel
        """
        logger.info("Making Prediction for :" + slack_message)

        schema = StructType([StructField('message', StringType(), True)])
        df = self.sqlContext.createDataFrame(self.sc.parallelize([[slack_message]]), schema)
        pipeline = self.__build_prediction_pipeline()
        pipelineFit = pipeline.fit(df)
        dataset = pipelineFit.transform(df)
        prediction = self.model.transform(dataset).select("prediction").collect()[0][0]
        return prediction

    def prediction_to_string(self, prediction):
        """Takes a label index and transforms it to the String version
        """

        channel_string = self.labels.get(int(prediction), "Something went wrong, no prediction for you :(")

        return channel_string

    def __init__(self, sc):
        """Init the classification engine given a Spark context
        """

        logger.info("Starting up the Classification Engine: ")

        self.sc = sc
        self.sqlContext = SQLContext(sc)

        # Load Messages to Train On
        logger.info("Loading Training data...")
        file_location = "/user/mcarlin/db/slack-data/clean-data.txt"
        self.raw_data = self.sqlContext.read.format('com.databricks.spark.csv').options(header=True, inferschema='true').load(
            file_location)

        self.__train_model()