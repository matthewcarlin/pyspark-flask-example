from engine import SlackClassificationEngine
from flask import Flask
from flask import Blueprint

api = Blueprint('api', 'api', url_prefix='/api')


@api.route("/<string:term>/", methods=["GET"])

def getMember(term):
    pred_num = recommendation_engine.predict_channel(term)
    pred_str = recommendation_engine.prediction_to_string(pred_num)

    return pred_str


def create_app(spark_context):
    global recommendation_engine

    recommendation_engine = SlackClassificationEngine(spark_context)

    app = Flask(__name__)
    app.register_blueprint(api)

    return app